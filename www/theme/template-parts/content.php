<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hiraeth
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php hiraeth_post_thumbnail(); ?>
	<div class="article-information">
		<header class="entry-header">
			<a href="<?php the_permalink() ?>" rel="bookmark">
				<?php the_title('<h1 class="entry-title">', '</h1>'); ?>
			</a>
		</header>

		<div class="entry-content">
			<?php the_excerpt(); ?>
		</div>

		<footer class="entry-footer">
			<?php hiraeth_posted_on(); ?>
		</footer>
	</div>
</article>
