<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package hiraeth
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, minimum-scale=1.0, user-scalable=yes">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="site">
		<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e(
				'Skip to content',
				'hiraeth'
			); ?></a>

		<header id="masthead" class="site-header">
			<nav id="site-navigation" class="main-navigation">
				<button
					class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"
				>☰</button>
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'menu-1',
						'menu_id' => 'primary-menu',
						"depth" => 1
					)
				);
				?>
			</nav><!-- #site-navigation -->

			<div id="site-branding">
				<div class="site-name">
					<h1 class="site-title">
						<a href="<?php echo esc_url(home_url('/')); ?>" rel="home"><?php bloginfo(
								'name'
							); ?></a>
					</h1>
					<?php
					$hiraeth_description = get_bloginfo('description', 'display');
					if ($hiraeth_description || is_customize_preview()) :
						?>
						<p class="site-description"><?php echo $hiraeth_description; /* WPCS: xss ok. */ ?></p>
					<?php endif; ?>
				</div>
				<div class="site-logo">
					<img
						alt="Hireath logo"
						src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo.png"
					>
				</div>
			</div><!-- .site-branding -->
		</header><!-- #masthead -->

		<div id="content" class="site-content">
