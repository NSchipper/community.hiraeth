<?php
/* Template Name: Home page */
/**
 * The template for displaying the home page of the hiraeth website.
 *
 * @package hiraeth
 */

get_header();
?>
	<aside id="events" class="events-area">
		<header>
			<h1 class="page-title">Recent logs:</h1>
		</header>

		<?php
		$response = wp_remote_get(esc_url_raw("http://api:80/warcraftlogs/reports"));
		$api_response = json_decode(wp_remote_retrieve_body($response), true);
		if (!empty($api_response)):
			// get the last 5
			$logs = array_slice($api_response, 0, 9);
			foreach ($logs as $log): ?>
				<article id="<?php echo $log['id']; ?>" class="log post">
					<div class="article-information">
						<header class="entry-header">
							<a
								href="<?php echo 'https://www.warcraftlogs.com/reports/' . $log['id'] ?>"
								rel="bookmark"
							>
								<h1 class="entry-title">
									<?php echo $log['title']; ?>
								</h1>
							</a>
						</header>

						<footer class="entry-footer">
							<span class="posted-on">
								<time
									class="entry-date published updated"
									datetime="<?php echo $log['start'] / 1000; ?>"
								>
									<?php echo date('D, d M Y H:i', $log['start'] / 1000); ?>
								</time>
							</span>
						</footer>
					</div>
				</article>
			<?php endforeach;
		endif;
		?>
	</aside>

	<div id="primary" class="content-area home">
		<main id="main" class="site-main">
			<?php while (have_posts()) :the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php hiraeth_post_thumbnail(); ?>

					<div class="entry-content">
						<?php the_content(); ?>
					</div>
				</article>
			<?php endwhile; ?>
		</main>
	</div>
<?php get_footer();
