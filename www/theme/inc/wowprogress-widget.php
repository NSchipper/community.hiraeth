<?php

class WoWProgressWidget extends WP_Widget {
	function __construct() {
		$widgetOptions = array("description" => "A simple widget to show the wow-progress rank image");
		parent::__construct("wow_progress_widget", "WoWProgress Widget", $widgetOptions);
	}

	/**
	 * Widget output in the pages
	 * @param $args
	 * @param $instance
	 */
	public function widget($args, $instance) {
		$imageUrl = $instance['imageUrl'];
		echo $args['before_widget'];
		echo <<<EOL
			<div class="wowprogress-widget">
				<img src="{$imageUrl}" />
			</div>
EOL;
		echo $args['after_widget'];
	}

	/**
	 * Widget form within the wordpress admin panel
	 * @param $instance
	 */
	public function form($instance) {
		$imageUrl = isset($instance['imageUrl']) ? $instance['imageUrl'] : "";
		$escapedTitle = esc_attr($imageUrl);

		echo <<<EOL
			<p>
				<label for="{$this->get_field_id('imageUrl')}">WowProgress image url</label>
				<input
					class="widefat"
					id="{$this->get_field_id('imageUrl')}"
					name="{$this->get_field_name('imageUrl')}"
					type="text"
					value="{$escapedTitle}"
				/>
			</p>
EOL;
	}

	public function update($new_instance, $old_instance) {
		$instance = array();
		$instance['imageUrl'] = (!empty($new_instance['imageUrl'])) ? strip_tags($new_instance['imageUrl'])
			: '';
		return $instance;
	}
}

function wow_progress_load_widget() {
	register_widget('WoWProgressWidget');
}
add_action('widgets_init', 'wow_progress_load_widget');