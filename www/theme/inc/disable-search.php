<?php
function hiraeth_filter_query($query, $error = true) {
	if (is_search()) {
		$query->is_search = false;
		$query->query_vars[s] = false;
		$query->query[s] = false;
		if ($error == true) {
			$query->is_404 = true;
		}
	}
}

function hiraeth_search_form() {
	return null;
}

function hiraeth_remove_search_widget() {
	unregister_widget('WP_Widget_Search');
}

add_action('parse_query', 'hiraeth_filter_query');
add_filter('get_search_form', 'hiraeth_search_form');
add_action('widgets_init', 'hiraeth_remove_search_widget');