import { Injectable, HttpService } from '@nestjs/common';
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { WarcraftlogsReport } from "./report.model";

@Injectable()
export class WarcraftlogsService {
	// docs can be found here: https://www.warcraftlogs.com/v1/docs/
	private readonly warcraftLogsApiBaseUrl = "https://www.warcraftlogs.com/v1";
	private readonly warcraftLogsApiGuild = "Hiraeth";
	private readonly warcraftLogsApiRealm = "argent-dawn";
	private readonly warcraftLogsApiServerRegion = "EU";
	private readonly apiKey: string = process.env.WARCRAFT_LOGS_PUBLIC_API_KEY;

	constructor(private readonly httpService: HttpService) {}

	getReports(): Observable<WarcraftlogsReport[]> {
		const url = `${this.warcraftLogsApiBaseUrl}/reports/guild/${this.warcraftLogsApiGuild}/${this.warcraftLogsApiRealm}/${this.warcraftLogsApiServerRegion}`;
		console.warn("remove me!", url, this.apiKey);
		return this.httpService.get(`${url}?api_key=${this.apiKey}`).pipe(map(response => response.data));
	}
}
