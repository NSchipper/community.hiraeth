export interface WarcraftlogsReport {
	id: string;
	title: string;
	owner: string;
	start: number;
	end: number;
	zone: number;
}