import { Module, CacheModule, HttpModule } from '@nestjs/common';
import { WarcraftlogsController } from './controller/warcraftlogs/warcraftlogs.controller';
import { WarcraftlogsService } from "./service/warcraftlogs/warcraftlogs.service";

@Module({
  imports: [CacheModule.register({
    ttl: 300, // 5 minutes
    max: 100, // maximum number of items in cache
  }), HttpModule],
  controllers: [WarcraftlogsController],
  providers: [WarcraftlogsService],
})
export class AppModule {}
