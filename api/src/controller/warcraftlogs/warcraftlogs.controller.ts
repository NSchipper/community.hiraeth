import { Controller, Get, UseInterceptors, CacheInterceptor } from '@nestjs/common';
import { WarcraftlogsReport } from "../../service/warcraftlogs/report.model";
import { WarcraftlogsService } from "../../service/warcraftlogs/warcraftlogs.service";
import { Observable } from "rxjs";

@Controller('warcraftlogs')
export class WarcraftlogsController {
  constructor(private readonly service: WarcraftlogsService) {}

  @Get('reports')
  @UseInterceptors(CacheInterceptor)
  getReports(): Observable<WarcraftlogsReport[]> {
    return this.service.getReports();
  }
}
