# Rest API

## Calendar API

### Create a new calendar event
POST: `/calendar/event/`

### Update an existing calendar event
UPDATE: `/calendar/event/<<event-id>>`

### Remove a calender event
DELETE: `/calendar/event/<<event-id>>`

### Sign up to calendar event
POST: `/calendar/event/<<event-id>>/member`

### Update sign up status for calendar event
UPDATE: `/calendar/event/<<event-id>>/member/<<member-id>>`

### Remove sign up for calendar event
DELETE: `/calendar/event/<<event-id>>/member/<<member-id>>`

## Login API

`/login`